package chromecookie

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"time"
)

// Chrome Cookie Export Format https://developer.chrome.com/extensions/cookies#type-Cookie
type JsonCookie struct {
	Name           string  `json:name`
	Value          string  `json:value`
	Path           string  `json:path`
	Domain         string  `json:domain`
	ExpirationDate float64 `json:expirationDate`
	Secure         bool    `json:secure`
	HttpOnly       bool    `json:httpOnly`
	SameSite       string  `json:sameSite`
	// unused / unknown
	HostOnly bool   `json:hostOnly`
	Session  bool   `json:session`
	StoreId  string `json:storeId`
	Id       int    `json:id`
}

func UnmarshalJSONCookies(jsonCookies []byte) ([]JsonCookie, error) {
	cookies := make([]JsonCookie, 10)
	err := json.Unmarshal(jsonCookies, &cookies)
	return cookies, err
}

func (jc *JsonCookie) ToHTTPCookie() *http.Cookie {
	rc := new(http.Cookie)

	rc.Name = jc.Name
	rc.Value = jc.Value

	rc.Path = jc.Path
	rc.Domain = jc.Domain
	epochsecs := int64(jc.ExpirationDate)
	rc.Expires = time.Unix(epochsecs, 0)

	// RawExpires string    // for reading cookies only
	// MaxAge   int
	// = 0

	rc.Secure = jc.Secure
	rc.HttpOnly = jc.HttpOnly

	if jc.SameSite == "no_restriction" {
		rc.SameSite = http.SameSiteLaxMode
	} else {
		rc.SameSite = http.SameSiteDefaultMode
	}

	return rc

	// Raw      string
	// Unparsed []string
}

func CookiesFromChromeJSON(cjson []byte) ([]*http.Cookie, error) {
	cookies, err := UnmarshalJSONCookies(cjson)
	if err != nil {
		errors.New("Cookie parse error: " + err.Error())
	}
	rv := make([]*http.Cookie, 0)
	for _, c := range cookies {
		rv = append(rv, c.ToHTTPCookie())
	}
	return rv, err
}

func MakeCookieJar(url *url.URL, cookies []*http.Cookie) *cookiejar.Jar {
	cookieJar, _ := cookiejar.New(nil)
	cookieJar.SetCookies(url, cookies)
	return cookieJar
}
